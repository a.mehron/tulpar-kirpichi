<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Продажа кирпича в Казани по самым низким ценам");
$APPLICATION->SetPageProperty("keywords", "Продажа кирпича в Казани по самым низким ценам");
$APPLICATION->SetPageProperty("title", "Продажа кирпича в Казани по самым низким ценам");
$APPLICATION->SetTitle("Главная");
?><section class="features">
                <div class="features__inner"><p><a name="adv"></a></p>
                    <h2 class="section-title features__title">
                        <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_whyTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>
                
                    </h2>
                    <h4 class="section-subtitle">
                       <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_whySubtitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>

                    </h4>
                    
                    <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "why_us", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "why_us",
        "IBLOCK_ID" => "1",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "dop_class",
            1 => "icon",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "why_us",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                    
                   </div>
                </section>
                


                <section class="product-choose">
                <div class="container"><p><a name="manuf"></a></p>
                    <div class="product-choose__inner">
                        <h2 class="section-title product-choose__title">
                            Мы работаем только с 
                            <div class="section-title__red-line">проверенными производителями</div>                    
                        </h2>
                        <h4 class="section-subtitle">
                            Поэтому гарантируем высокое качество продукции и своевременную доставку на объект
                        </h4>
                        <div class="company-list wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            <div class="company-list__item" data-company="Алексеевская">
                                <img class="company-list__img" src="/local/templates/main/images/company/alekseevsk.png" alt="">
                            </div>
                            <div class="company-list__item" data-company="Альтаир">
                                <img class="company-list__img" src="/local/templates/main/images/company/altair.png" alt="">
                            </div>
                            <div class="company-list__item active" data-company="АСПК">
                                <img class="company-list__img" src="/local/templates/main/images/company/aspk.png" alt="">
                            </div>
                            <div class="company-list__item" data-company="КСКЕРАМИК">
                                <img class="company-list__img" src="/local/templates/main/images/company/kc_keramik.png" alt="">
                            </div>
                            <div class="company-list__item" data-company="Кощаковский">
                                <img class="company-list__img" src="/local/templates/main/images/company/koshak.png" alt="">
                            </div>
                            <div class="company-list__item" data-company="Магма">
                                <img class="company-list__img" src="/local/templates/main/images/company/magma.png" alt="">
                            </div>
                        </div>
</div>
</div>
                    </section>

                    <!-- 
                        <div class="product-settings product-choose__product-settings wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            <div class="product-settings__title">
                                Параметры:
                            </div>
                            <form class="product-settings__form">
                                <div class="product-settings__container product-settings__container--type">
                                    <div class="product-settings__container-title">
                                        Тип: 
                                    </div>
                                    <div class="product-settings__radio-btns product-settings__radio-btns--type">
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-type" id="type1" checked>  
                                            <label class="product-settings__label" for="type1">
                                                U-образный блок                                          
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-type" id="type2">  
                                            <label class="product-settings__label" for="type2">
                                                Крупноформатный блок                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-type" id="type3">  
                                            <label class="product-settings__label" for="type3">
                                                Перегородочный блок                                          
                                            </label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="product-settings__container">
                                    <div class="product-settings__container-title">
                                        Марка плотности: 
                                    </div>
                                    <div class="product-settings__radio-btns product-settings__radio-btns--mark">
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d300" checked>  
                                            <label class="product-settings__label" for="d300">
                                                D300                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d350">  
                                            <label class="product-settings__label" for="d350">
                                                D350                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d400">  
                                            <label class="product-settings__label" for="d400">
                                                D400                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d500">  
                                            <label class="product-settings__label" for="d500">
                                                D500                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d600">  
                                            <label class="product-settings__label" for="d600">
                                                D600                                        
                                            </label>
                                        </div>
                                        <div class="product-settings__item">
                                            <input class="product-settings__radio" type="radio" name="product-mark" id="d700">  
                                            <label class="product-settings__label" for="d700">
                                                D700                                        
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="product-item product-choose__product-item wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            <div class="product-item__inner">
                                <div class="product-item__img-block">
                                    <img class="product-item__img" src="/local/templates/main/images/manufacturer-item/u-block.png" alt="">
                                </div>
                                <div class="product-item__text-block">
                                    <div class="product-item__manufacturer">
                                        Производитель:
                                        <span class="product-item__manufacturer-item">Коттедж</span>
                                    </div> 
                                    <div class="product-item__leftside">                                    
                                        <div class="product-item__title">
                                            <span class="product-item__title--type">U-образный блок</span>                                             
                                            <span class="product-item__title--manufacturer">Коттедж</span>                                             
                                            <span class="product-item__title--mark">D300</span>                                             
                                            <span class="product-item__title--size">625*200*250</span>  
                                        </div> 
                                        <div class="product-item__min-for-order">
                                            Минимальное кол-во для покупки - <span class="product-item__min-for-order-item">1</span> поддон
                                        </div>
                                        <div class="product-item__price">
                                            <span>4 050</span>руб.
                                        </div>
                                    </div>  
                                    <div class="product-item__rightside">
                                        <div class="product-item__specifications">
                                            <div class="product-item__specifications-title">
                                                Характеристики:
                                            </div>
                                            <table class="product-item__specifications-table">
                                                <tr class="product-item__specifications-tr">
                                                    <th class="product-item__specifications-th">
                                                        Марка плотности:
                                                    </th>
                                                    <td class="product-item__specifications-td product-item__specifications-td--mark">
                                                        D300
                                                    </td>
                                                </tr>
                                                <tr class="product-item__specifications-tr">
                                                    <th class="product-item__specifications-th">
                                                        Кол-во на поддоне, шт: 
                                                    </th>
                                                    <td class="product-item__specifications-td">
                                                        64
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <button class="btn btn--red product-item__order" data-order-form>
                                            Заказать
                                        </button>
                                    </div>                   
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                
            </section>

-->
                
            <section class="special-price"><p><a name="warr"></a></p>                
                <h2 class="special-price__title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                    Нужен кирпич по специальной цене?
                </h2>
                <h4 class="special-price__subtitle wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                    Оставьте заявку и наш менеджер расскажет подробности акции
                </h4>
                <form class="form-item special-price__form-item wow fadeInUp" action="/local/templates/main/php/form1.php" method="post" data-wow-duration="0.8s" data-wow-delay="0s">
                    <input class="form-item__input special-price__input" type="text" name="number[]" placeholder="Телефон" required>
                    <button class="form-item__submit" type="submit">
                        <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-right"></use></svg>
                    </button>
                    <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                </form>
            </section>
                
                

                
                
                <section class="manufacturers">
                <div class="manufacturers__inner">
                    <h2 class="section-title product-choose__title">
                        <div class="section-title__red-line">Мы предлагаем только качественную продукцию</div>    
                        
                    </h2>
                    <h4 class="section-subtitle">
                        Поэтому можем быть на 100% уверены за конечный результат
                    </h4>
                    <?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list", 
    "catalog", 
    array(
        "COMPONENT_TEMPLATE" => "catalog",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "3",
        "SECTION_ID" => $_REQUEST["SECTION_ID"],
        "SECTION_CODE" => "",
        "COUNT_ELEMENTS" => "N",
        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
        "TOP_DEPTH" => "2",
        "SECTION_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "FILTER_NAME" => "sectionsFilter",
        "SECTION_URL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "CACHE_FILTER" => "N",
        "ADD_SECTIONS_CHAIN" => "N"
    ),
    false
);?>
                    
                    
                    </div>
                    </section>
                    
                    <section class="help">
                <div class="help__inner">
                    <h2 class="help__title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                        <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_helpTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>

                    </h2>
                    <h4 class="help__subtitle wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                        <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_helpSubtitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>

                    </h4>
                    <form class="help__form wow fadeInUp" action="/local/templates/main/php/form2.php" method="post" data-wow-duration="0.8s" data-wow-delay="0s">
                        <input class="input help__input" type="text" name="name[]" placeholder="Имя">
                        <input class="input help__input" type="text" name="number[]" placeholder="Телефон">
                        <button class="btn btn--red help__submit" type="submit">
                            Получить консультацию
                        </button>
                        <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                    </form>
                    <img class="help__img-bg wow fadeInUp" src="/local/templates/main/images/help/bg-img.png" alt="" data-wow-duration="0.8s" data-wow-delay="0s">
                </div>
            </section>
            <section class="economy-mode">
                <div class="economy-mode__inner">
                    <h2 class="economy-mode__title">
                        ВКЛЮЧИТЬ ЭКОНОМНЫЙ РЕЖИМ
                    </h2>
                    <input class="economy-mode__input" type="checkbox" id="toggle-mode">
                    <label class="economy-mode__toggle" for="toggle-mode" data-economy-mode></label>
                </div>
            </section>
            
            
            
            
             <section class="low-price">
                <div class="low-price__inner">
                    <div class="low-price__img-block wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0s">
                        <img class="low-price__img" src="/local/templates/main/images/low-price/1.png" alt="">
                    </div>
                    <div class="low-price__text-block wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0s">
                        <h4 class="low-price__title ">
                             <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_lowpriceTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>
                        </h4>
                        <p class="low-price__desc">
                           <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_lowpriceSubTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>
                        </p>
                        <p class="low-price__order">
                            Оставьте заявку и убедитесь в этом сами
                        </p>
                        <form class="form-item low-price__form-item" action="/local/templates/main/php/form1.php" method="post">
                            <input class="form-item__input" type="text" name="number[]" placeholder="Телефон" required>
                            <button class="form-item__submit" type="submit">
                                <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-right"></use></svg>
                            </button>
                            <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                        </form>
                    </div>
                </div>
            </section>
            
            
            <section class="order-stage"><p><a name="stg"></a></p>
                <div class="order-stage__inner">
                    <h2 class="section-title order-stage__title">
                        <div class="section-title__red-line order-stage__red-line">
                        <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_orderTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>
                        </div>    
                    </h2>
                    <h4 class="section-subtitle order-stage__subtitle">
                        <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc_orderSubTitle",
"EDIT_MODE" => "html",
"EDIT_TEMPLATE" => ""
),
false
);?>
                    </h4>
                    <div class="order-stage__btns-nav">
                        <div class="order-stage__btn-nav order-stage__btn-nav--prev">
                            <svg class="order-stage__btn-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-left"></use></svg>
                        </div>
                        <div class="order-stage__btn-nav order-stage__btn-nav--next">
                            <svg class="order-stage__btn-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-right"></use></svg>
                        </div>
                    </div>
                    <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "order", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "managers",
        "IBLOCK_ID" => "4",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "num",
            1 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "order",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                    
                    </div>
                    </section>
                    
                    
                    <section class="video-section">
                <div class="video-section__inner wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                    <h2 class="video-section__title">
                        Узнать больше о нас в этом видео 
                    </h2>
                    <div class="video-more__link-container video-section__link-container">                        
                        <svg class="video-more__icon-arrow-right video-section__icon-arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-video-right"></use></svg>
                        <a class="video-more__link video-section__link" data-fancybox href="https://www.youtube.com/watch?v=-nfcl8Y-yDo">
                            <svg class="video-more__icon-play video-section__icon-play"><use xlink:href="/local/templates/main/images/sprite.svg#icon-play"></use></svg>
                        </a>
                    </div>
                </div>
            </section>
            
            
             <section class="price-graph">
                <div class="price-graph__inner">
                    <h2 class="section-title price-graph__title">
                        Цены в Тулпар ниже 
                        <div class="section-title__red-line price-graph__red-line">чем у других компаний </div>   
                    </h2>
                    <h4 class="section-subtitle price-graph__subtitle">
                        Оцените масштаб нашей работы 
                    </h4>
                    <div class="price-graph__wrapper">
                        <aside class="price-graph__aside wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0s">
<!--
                            <div class="graph-aside-item price-graph__aside-item">
                                <div class="graph-aside-item__img-block">
                                    <img class="graph-aside-item__img" src="/local/templates/main/images/manufacturer-item/1.png" alt="">
                                    <div class="graph-aside-item__product-name">
                                        Лицевой кирпич
                                    </div>
                                </div>
                                
                                <div class="graph-aside-item__text-block">
                                    <p class="graph-aside-item__text">
                                        Мы более 7 лет на рынке поэтому 
                                        мы можем гарантировать, что у нас только самый качественный продукт
                                    </p>
                                </div>
                            </div>
                            <div class="graph-aside-item price-graph__aside-item">
                                <div class="graph-aside-item__img-block">
                                    <img class="graph-aside-item__img" src="/local/templates/main/images/manufacturer-item/1.1.png" alt="">
                                    <div class="graph-aside-item__product-name">
                                        Цокольный кирпич
                                    </div>
                                </div>
                                
                                <div class="graph-aside-item__text-block">
                                    <p class="graph-aside-item__text">
                                        Чтобы всегда предлагать самые низкие цены, мы работаем только напрямую с заводами производителями, на эксклюзивных условиях.
                                    </p>
                                </div>
                            </div>
-->
                        </aside>
                        <div class="graph">
                            <div class="graph__wrapper">
                                <div class="graph__inner wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                                    <div class="graph__item">
                                        <div class="graph__value">
                                            2012
                                        </div>
                                        <div class="graph__scale " style="max-width: 10%;"></div>
                                    </div>
                                    <div class="graph__item">
                                        <div class="graph__value">
                                            2015
                                        </div>
                                        <div class="graph__scale " style="max-width: 40%;"></div>
                                    </div>
                                    <div class="graph__item">
                                        <div class="graph__value">
                                            2020
                                        </div>
                                        <div class="graph__scale " style="max-width: 100%;"></div>
                                    </div>
                                </div>
                                <div class="graph__line-container">
                                    <svg class="graph__line"><use xlink:href="/local/templates/main/images/sprite.svg#icon-graph-line"></use></svg>
                                    <div class="graph__line-text">
                                        <div class="graph__line-value graph__line-value--start">
                                            0
                                        </div>
                                        <div class="graph__line-value graph__line-value--end">
                                            37 млн шт
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="low-price-form wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                <div class="low-price-form__inner">
                    <div class="low-price-form__content">
                        <h2 class="low-price-form__title">
                            Столько продукции не производит ни одна компания, поэтому цены <br> очень  низкие
                        </h2>
                        <h4 class="low-price-form__subtitle">
                            Отправьте заявку и убедитесь в этом сами
                        </h4>
                        <form class="form-item low-price-form__form-item" action="/local/templates/main/php/form1.php" method="post">
                            <input class="form-item__input" type="text" placeholder="Телефон" name="number[]" required>
                            <button class="form-item__submit" type="submit">
                                <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-right"></use></svg>
                            </button>
                            <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                        </form>
                    </div>
                    <img class="low-price-form__img" src="/local/templates/main/images/low-price-form/1.png" alt="">
                </div>
            </section>
            <section class="material">
                <div class="material__inner">
                    <div class="material__header">
                        <h2 class="material__title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            У нас есть материал подходящий для вас
                        </h2>
                        <div class="material__wrapper wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            <h4 class="material__subtitle">
                                Чтобы не ошибиться в подборе материала оставьте заявку
                            </h4>
                            <form class="form-item material__form-item" action="/local/templates/main/php/form1.php" method="post">
                                <input class="form-item__input" type="text" placeholder="Телефон" name="number[]" required>
                                <button class="form-item__submit" type="submit">
                                    <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-right"></use></svg>
                                </button>
                                <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                            </form>
                        </div>
                    </div>
                   <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "material", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "material",
        "IBLOCK_ID" => "5",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "material",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                </div>
            </section>
            <section class="gas-blocks">
                <div class="gas-blocks__inner">
                    <h2 class="section-title">
                        <div class="section-title__red-line">Кирпичи Тулпар уже в домах <br> наших клиентов</div>   
                    </h2>
                    <h4 class="section-subtitle ">
                        Дома, построенные из нашего кирпича
                    </h4>
                    <div class="gas-blocks__slider">
                        <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "houses", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "houses",
        "IBLOCK_ID" => "6",
        "NEWS_COUNT" => "200",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "houses",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                    </div>
                    
                </div>
            </section>
            <section class="home-owner">
                <div class="home-owner__inner">
                    <h2 class="home-owner__title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                        Хотите стать счастливым обладателем <br>  дома?
                    </h2>
                    <div class="home-owner__request wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                        Оставьте заявку
                    </div>
                    <form class="form-item home-owner__form-item wow fadeInUp" action="/local/templates/main/php/form1.php" method="post" data-wow-duration="0.8s" data-wow-delay="0s">
                        <input class="form-item__input" type="text" placeholder="Телефон" name="number[]" required>
                        <button class="form-item__submit" type="submit">
                            <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-right"></use></svg>
                        </button>
                        <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                    </form>
                </div>
            </section>
            <section class="compare-prices">
                <div class="compare-prices__inner">
                    <h2 class="section-title compare-prices__title wow fadeInUp" data-material data-wow-duration="0.8s" data-wow-delay="0s">
                        <div class="section-title__red-line">Сравните цены с конкурентами</div>   
                    </h2>
                    <h4 class="section-subtitle compare-prices__subtitle wow fadeInUp" data-material data-wow-duration="0.8s" data-wow-delay="0s">
                        Дома, построенные из нашего газоблокаго газоблока
                    </h4>
                    <div class="compare-prices__table-container wow fadeInUp" data-material data-wow-duration="0.8s" data-wow-delay="0s">
                        <table class="compare-prices__table">
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    
                                </th>
                                <th class="compare-prices__th">
                                    Завод
                                </th>
                                <th class="compare-prices__th">
                                    База материалов
                                </th>
                                <th class="compare-prices__th">
                                    Официальный дилер
                                </th>
                            </tr>
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    Высокое качесвто продукции
                                </th>
                                <td class="compare-prices__td">
                                   <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                    </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                            </tr>
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    Быстрая доставка
                                </th>
                                <td class="compare-prices__td">
                                   <div class="close">
                                        <svg class="close__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-close"></use></svg>
                                   </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                    </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                            </tr>
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    Гарантия производителя
                                </th>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                    </div>                                   
                                </td>
                                <td class="compare-prices__td">
                                    <div class="close">
                                        <svg class="close__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-close"></use></svg>
                                   </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                            </tr>
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    Точный расчет материала
                                </th>
                                <td class="compare-prices__td">
                                    <div class="close">
                                        <svg class="close__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-close"></use></svg>
                                   </div>                                  
                                </td>
                                <td class="compare-prices__td">
                                    <div class="close">
                                        <svg class="close__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-close"></use></svg>
                                   </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                            </tr>
                            <tr class="compare-prices__tr">
                                <th class="compare-prices__th">
                                    Помощь в подборе материалов
                                </th>
                                <td class="compare-prices__td">
                                    <div class="close">
                                        <svg class="close__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-close"></use></svg>
                                   </div>                                  
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                                <td class="compare-prices__td">
                                    <div class="check">
                                        <svg class="check__icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-check"></use></svg>
                                   </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </section>
            <section class="reviews">
                <div class="reviews__inner">
                    <h2 class="section-title">
                        <div class="section-title__red-line">Более 90 % клиентов  Тулпар рекомендуют нас <br> своим друзьям и знакомым</div>   
                    </h2>
                    <h4 class="section-subtitle">
                        Вы всегда можете посмотреть отзывы о нас в Интернете
                    </h4>
                    <div class="reviews__slider">
                        <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "reviews", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "reviews",
        "IBLOCK_ID" => "7",
        "NEWS_COUNT" => "2000",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "video",
            1 => "logo",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "reviews",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                                </div>
                                
                                
                                
                                
                    </div>
                </div>
            </section>
            <section class="social">
                <div class="social__inner">
                    <div class="social__title-block">
                        <h2 class="section-title">
                            <div class="section-title__red-line social__red-line">Мы всегда открыты <br> для наших клиентов</div>   
                        </h2>
                        <h4 class="section-subtitle">
                            Мы активно ведем социальные сети 
                            и даем советы каждый день
                        </h4>
                    </div>
                    <div class="social__wrapper">
                        <?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "open", 
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "open",
        "IBLOCK_ID" => "8",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "icon",
            1 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DISPLAY_PANEL" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "open",
        "CACHE_GROUPS" => "Y",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "FILE_404" => ""
    ),
    false
);?>
                    </div>
                </div>
            </section>
            <section class="offer">
                <div class="offer__inner">
                    <div class="offer__container">
                        <h4 class="offer__title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                            Получите коммерческое предложение уже сегодня
                        </h4>
<!--
                        <a class="offer__download wow fadeInUp" href="#" data-wow-duration="0.8s" data-wow-delay="0s">
                            Скачать
                        </a>
-->
                        <form class="offer__form wow fadeInUp" action="/local/templates/main/php/form1.php" method="post" data-wow-duration="0.8s" data-wow-delay="0s">
                            <input class="input offer__input" type="text" placeholder="Телефон" name="number[]" required>
                            <button class="btn btn--red offer__submit" type="submit">
                                Заказать
                            </button>
                            <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                        </form>
                    </div>
                </div>
            </section>
            <div class="bottom-blocks"><p><a name="contacts"></a></p>
                <section class="contacts wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0s">
                    <div class="social-links">
                        <a class="social-links__link" href="https://ok.ru/group/55773038379124">
                            <svg class="social-links__icon social-links__icon--odnoklassniki"><use xlink:href="/local/templates/main/images/sprite.svg#icon-odnoklassniki"></use></svg>
                        </a>
                        <a class="social-links__link" href="https://www.facebook.com/tulparooo/?modal=admin_todo_tour">
                            <svg class="social-links__icon social-links__icon--facebook"><use xlink:href="/local/templates/main/images/sprite.svg#icon-facebook"></use></svg>
                        </a>
                        <a class="social-links__link" href="https://www.instagram.com/tulpar_trade/">
                            <svg class="social-links__icon social-links__icon--instagram"><use xlink:href="/local/templates/main/images/sprite.svg#icon-instagram"></use></svg>
                        </a>
                        <a class="social-links__link" href="https://www.youtube.com/channel/UCVORTWcbjF3oZf-A2uTQCIg">
                            <svg class="social-links__icon social-links__icon--youtube"><use xlink:href="/local/templates/main/images/sprite.svg#icon-youtube"></use></svg>
                        </a>
                        <a class="social-links__link" href="https://vk.com/tulpar_trade">
                            <svg class="social-links__icon social-links__icon--vk"><use xlink:href="/local/templates/main/images/sprite.svg#icon-vk"></use></svg>
                        </a>
                    </div>
                    <div class="contacts__inner">
                        <h5 class="contacts__title">
                            Контакты
                        </h5>
                        <div class="contacts__container">
                            <div class="contacts__item">   
                                <div class="contacts__icon-block">
                                    <svg class="contacts__icon contacts__icon--phone-footer"><use xlink:href="/local/templates/main/images/sprite.svg#icon-phone-footer"></use></svg>
                                </div>
                                <div class="contacts__text-block">
                                    <div class="contacts__item-title">
                                        Отдел продаж
                                    </div>
                                    <a class="contacts__content" href="tel:+78432163267">
                                        +7 (843) 207-32-83
                                    </a>
                                </div>
                            </div>
                            <div class="contacts__item">   
                                <div class="contacts__icon-block">
                                    <svg class="contacts__icon contacts__icon--clock"><use xlink:href="/local/templates/main/images/sprite.svg#icon-clock"></use></svg>
                                </div>
                                <div class="contacts__text-block">
                                    <div class="contacts__item-title">
                                        Режим работы
                                    </div>
                                    <div class="contacts__content" href="tel:+78432163267">
                                        Пн - Пт: 08:00 - 19:00  <br>
                                        Сб: 09:00 - 14:00
                                    </div>
                                </div>
                            </div>
                            <div class="contacts__item">   
                                <div class="contacts__icon-block">
                                    <svg class="contacts__icon contacts__icon--mark"><use xlink:href="/local/templates/main/images/sprite.svg#icon-mark"></use></svg>
                                </div>
                                <div class="contacts__text-block">
                                    <div class="contacts__item-title">
                                        Адрес
                                    </div>
                                    <div class="contacts__content">
                                        г. Казань, ул. Даурская 12а, этаж 3, <br>
                                        офис 20.4
                                    </div>
                                </div>
                            </div>
                            <div class="contacts__item">   
                                <div class="contacts__icon-block">
                                    <svg class="contacts__icon contacts__icon--mail"><use xlink:href="/local/templates/main/images/sprite.svg#icon-mail"></use></svg>
                                </div>
                                <div class="contacts__text-block">
                                    <div class="contacts__item-title">
                                        E-mail
                                    </div>
                                    <a class="contacts__content" href="mailto:tulpar-trade@mail.ru">
                                        tulpar-trade@mail.ru
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="map wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0s">
                    <iframe class="map__iframe" src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa5b2ab24f9dd5bb6e4b65228e49a9263f42c11f16a23e4efc8b5edb91009e82f&amp;source=constructor" width="713" height="525" frameborder="0"></iframe>
                    
                </div>
            </div><?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>