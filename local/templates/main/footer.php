 </main>
        <footer class="footer">
            <div class="footer__inner">
                <div class="footer__copyright-block footer__container">
                    <a class="logo footer__logo" href="">
                        <img class="logo__img logo__img--footer" src="<?= SITE_TEMPLATE_PATH ?>/images/logo-mobile.svg" alt="">
                    </a>
                    <div class="footer__copyright">
                        © Все права защищены <br>
                        ООО Тулпар 2012-2020 
                    </div>
                </div>
                <div class="footer__container footer__container--list">
                    <ul class="footer__list">
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Производители
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Этапы работы       
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Цены        
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Наши преимущества        
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Производители      
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Контакты      
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Отзывы      
                            </a>
                        </li>
                        <li class="footer__list-item">
                            <a class="footer__list-link" href="#">
                                Этапы работы         
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="footer__container">
                    <div class="contacts">                        
                        <div class="contacts__inner footer__contacts-inner">                            
                            <div class="contacts__container footer__contacts-container">
                                <div class="contacts__item footer__contacts-item">   
                                    <div class="contacts__icon-block">
                                        <svg class="contacts__icon footer__contacts-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-phone-footer"></use></svg>
                                    </div>
                                    <div class="contacts__text-block">
                                        <div class="contacts__item-title">
                                            Отдел продаж
                                        </div>
                                        <a class="contacts__content" href="tel:+78432163267">
                                            +7 (843) 207-32-83
                                        </a>
                                    </div>
                                </div>
                                <div class="contacts__item footer__contacts-item">   
                                    <div class="contacts__icon-block">
                                        <svg class="contacts__icon footer__contacts-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-mail"></use></svg>
                                    </div>
                                    <div class="contacts__text-block">
                                        <div class="contacts__item-title">
                                            E-mail
                                        </div>
                                        <a class="contacts__content" href="mailto:tulpar-trade@mail.ru">
                                            tulpar-trade@mail.ru
                                        </a>
                                    </div>
                                </div>
                                <div class="contacts__item footer__contacts-item">   
                                    <div class="contacts__icon-block">
                                        <svg class="contacts__icon footer__contacts-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-clock"></use></svg>
                                    </div>
                                    <div class="contacts__text-block">
                                        <div class="contacts__item-title">
                                            Режим работы
                                        </div>
                                        <div class="contacts__content" href="tel:+78432163267">
                                            Пн - Пт: 08:00 - 19:00  <br>
                                            Сб: 09:00 - 14:00
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts__item footer__contacts-item">   
                                    <div class="contacts__icon-block">
                                        <svg class="contacts__icon footer__contacts-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-mark"></use></svg>
                                    </div>
                                    <div class="contacts__text-block">
                                        <div class="contacts__item-title">
                                            Адрес
                                        </div>
                                        <div class="contacts__content">
                                            г. Казань, ул. Даурская 12а, этаж 3,
                                            офис 20.4
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="footer__container">
                    <div class="social-links footer__social-links">
                        <a class="social-links__link footer__social-link" href="https://ok.ru/group/55773038379124">
                            <svg class="social-links__icon footer__social-links-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-odnoklassniki"></use></svg>
                        </a>
                        <a class="social-links__link footer__social-link" href="https://www.youtube.com/channel/UCVORTWcbjF3oZf-A2uTQCIg">
                            <svg class="social-links__icon footer__social-links-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-youtube"></use></svg>
                        </a>
                        <a class="social-links__link footer__social-link" href="https://www.facebook.com/tulparooo/?modal=admin_todo_tour">
                            <svg class="social-links__icon footer__social-links-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-facebook"></use></svg>
                        </a>
                        <a class="social-links__link footer__social-link" href="https://www.instagram.com/tulpar_trade/">
                            <svg class="social-links__icon footer__social-links-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-instagram"></use></svg>
                        </a>
                        <a class="social-links__link footer__social-link" href="https://vk.com/tulpar_trade">
                            <svg class="social-links__icon footer__social-links-icon"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-vk"></use></svg>
                        </a>

                        <!-- <a class="social-links__link footer__social-link footer__social-link--yandex" href="#">
                           Я
                        </a> -->

                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    
    
    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"why_us_footer", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "why_us",
		"IBLOCK_ID" => "1",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "title",
			1 => "text",
			2 => "form_title",
			3 => "dop_class",
			4 => "banner",
			5 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DISPLAY_PANEL" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "why_us_footer",
		"CACHE_GROUPS" => "Y",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "Y",
		"FILE_404" => ""
	),
	false
);?>
    
    
    <div class="modal modal-sps">
        
            <div class="modal-order__title">
                Спасибо за ваше сообщение
            </div>
            <div class="modal-order__product-title">
                Мы свяжемся с Вами в ближайшее время
            </div>
           
       
    </div>
    
    
    <div class="modal modal-order">
        <form class="modal-order__body">
            <div class="modal-order__title">
                Форма заказа
            </div>
            <div class="modal-order__product-title">
                Наименование товара
            </div>
            <input class="input modal-order__input" type="text" name="product-name" placeholder="Газобетонный блок Коттедж D600" required>
            <input class="input modal-order__input" type="text" name="name" placeholder="Имя" required>
            <input class="input modal-order__input" type="text" name="phone" placeholder="Телефон" required>
            <button class="btn btn--red modal-order__submit" type="submit">
                Заказать
            </button>
            <div class="modal-order__consent">
                Нажимая кнопку, вы даёте согласие на отправку персональных данных. Все данные защищены 
                от передачи третьим лицам.
            </div>
        </form>
    </div>
    <div class="modal modal-ecomomy-mode">
        <div class="modal-ecomomy-mode__body">
            <img class="modal-ecomomy-mode__img" src="<?= SITE_TEMPLATE_PATH ?>/images/economy-mode/img-modal-1.png" alt="">
            <div class="modal-ecomomy-mode__text-block">
                <div class="modal-ecomomy-mode__timer">
                    00:30
                </div>
                <div class="modal-ecomomy-mode__money">
                    Минус 1000 рублей 
                </div>
                <div class="modal-ecomomy-mode__desc">
                    КУПИ ГАЗОБЛОКИ ПОЛУЧИ 
                    <span class="modal-ecomomy-mode__desc--red">КЛЕЙ</span> 
                    И <span class="modal-ecomomy-mode__desc--red">БЕСПЛАТНУЮ ДОСТАВКУ</span> В ПОДАРОК
                </div>
                <form class="form-item modal-ecomomy-mode__form-item">
                    <input class="form-item__input" type="text" placeholder="Телефон" required>
                    <button class="form-item__submit" type="submit">
                        <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-right"></use></svg>
                    </button>
                </form>
            </div>
            <img class="modal-ecomomy-mode__img" src="<?= SITE_TEMPLATE_PATH ?>/images/economy-mode/img-modal-2.png" alt="">
        </div>
    </div>
    <div class="modal modal-material">
        <div class="modal-material__body" style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/features/managers-modal.jpg);">
            <div class="modal-material__inner">
                <h2 class="modal-material__title">
                    У нас есть
                    <span class="modal-material__red-text">материал подходящий</span> 
                    для вас
                </h2>
                <h4 class="modal-material__subtitle">
                    
                </h4>
                <h4 class="modal-material__desc">
                    Оставьте заявку и убедитесь в этом сами
                </h4>
                <form class="form-item modal-material__form-item" action="/local/templates/main/php/form1.php" method="post">
                    <input class="form-item__input" type="text" placeholder="Телефон" name="number[]" required>
                    <button class="form-item__submit" type="submit">
                        <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-right"></use></svg>
                    </button>
                    <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-present">
        <div class="modal-present__body">
            <img class="modal-present__img modal-present__img--brick" src="<?= SITE_TEMPLATE_PATH ?>/images/brick.png" alt="">
            <div class="modal-present__text-block">
                <div class="modal-present__title">
                    При заказе сегодня клей <br> для газобетона в <br> подарок! 
                </div>
                <form class="form-item modal-present__form-item">
                    <input class="form-item__input" type="text" placeholder="Телефон" required>
                    <button class="form-item__submit" type="submit">
                        <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-right"></use></svg>
                    </button>
                </form>
            </div>
            <img class="modal-present__img" src="<?= SITE_TEMPLATE_PATH ?>/images/present-large.png" alt="">
        </div>
    </div>
    <div class="modal modal-review-video">
        <div class="modal-review-video__inner">
            <div class="modal-review-video__wrapper">
                <iframe class="modal-review-video__iframe" width="100%" height="auto" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div> 
    <div class="present map__present">
        <img class="present__img" src="<?= SITE_TEMPLATE_PATH ?>/images/present.png" alt="">
        <a class="btn btn--red present__btn" href="#" data-present>
            Получить подарок
        </a>
    </div>   
</body>
</html>