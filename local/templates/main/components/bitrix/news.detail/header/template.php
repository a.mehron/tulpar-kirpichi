<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?> <div class="hero" <? if(!empty($arResult['PROPERTIES']['bg']['VALUE'])){?>style="background-image:url(<? echo CFile::GetPath($arResult["PROPERTIES"]["bg"]["VALUE"]) ?>)"<?}?>>
            <header class="header">
                <div class="header__inner">
                    <a class="logo" href="/">
                        <img class="logo__img" src="<? if(!empty($arResult['PROPERTIES']['logo']['VALUE'])){?><? echo CFile::GetPath($arResult["PROPERTIES"]["logo"]["VALUE"]) ?><?}else{?>/local/templates/main/images/logo.png<?}?>" alt="">
                        <img class="logo__img logo__img--mobile" src="<? if(!empty($arResult['PROPERTIES']['mob_logo']['VALUE'])){?><? echo CFile::GetPath($arResult["PROPERTIES"]["mob_logo"]["VALUE"]) ?><?}else{?>/local/templates/main/images/logo-mobile.svg<?}?>" alt="">
                    </a>
                    <nav class="menu">
                        <ul>
                            <li>
                                <a href="#warr">
                                    Гарантия низкой цены
                                </a>
                            </li>
                            <li>
                                <a href="#stg">
                                    Этапы заказа
                                </a>
                            </li>
                            <li>
                                <a href="#adv">
                                    Наши преимущества
                                </a>
                            </li>
                            <li>
                                <a href="#manuf">
                                    Производители
                                </a>
                            </li>
                            <li>
                                <a href="#contacts">
                                    Контакты
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="header-phone">
                        <a class="header-phone__item" href="tel:<?=$arResult['PROPERTIES']['phone_link']['VALUE']?>">
                            <span class="header-phone__icon-container">
                                <svg class="header-phone__icon header-phone__icon--phone"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-phone"></use></svg>
                            </span>
                            <span class="header-phone__number">
                                <?=$arResult['PROPERTIES']['phone']['VALUE']?>
                            </span>
                        </a>
                    </div>
                    <button class="burger">
                        <div class="burger__inner">
                            <div class="burger__line"></div>
                            <div class="burger__line"></div>
                            <div class="burger__line"></div>
                        </div>
                    </button>
                </div>
                
            </header>
            <div class="hero__inner">
                <h1 class="page-title wow slideInLeft"  data-wow-duration="0.5s" data-wow-delay="0s">
                    <?=htmlspecialcharsBack($arResult["PROPERTIES"]["title"]["VALUE"]["TEXT"])?>
                </h1>
                <ol class="hero__list wow slideInLeft"  data-wow-duration="0.5s" data-wow-delay="0s">
                
                <?foreach($arResult["DISPLAY_PROPERTIES"]["list"]["VALUE"] as $k=>$value):?>   
                    <li class="hero__list-item">
                        <?=$value?><?=$arItem["DISPLAY_PROPERTIES"]["list"]["DESCRIPTION"][$k]?>
                    </li>
                    <?endforeach?>
                    
                </ol>
                <form class="form hero__form wow slideInLeft" action="/local/templates/main/php/form1.php" method="post" data-wow-duration="0.5s" data-wow-delay="0s">
                    <h3 class="hero__form-title">
                       <?=$arResult['PROPERTIES']['form1_title']['VALUE']?>
                    </h3>
                    <div class="form-item hero__form-item" >
                        <input class="form-item__input" type="text" name="number[]" placeholder="Телефон" required>
                        <button class="form-item__submit" type="submit">
                            <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-right"></use></svg>
                        </button>
                    </div>
                    <input autocomplete="off" type="hidden" name="call-control[]" class="call-control" value="0">
                </form>
                <? if(!empty($arResult['PROPERTIES']['video']['VALUE'])){?>
                <div class="video-more wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0s">
                    <div class="video-more__inner">
                        <h3 class="video-more__title">
                            Узнать больше о нас в этом видео 
                        </h3>
                        <div class="video-more__link-container">                        
                            <svg class="video-more__icon-arrow-right arrow-animate"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-video-right"></use></svg>
                            <a class="video-more__link" href="<?=htmlspecialcharsBack($arResult["PROPERTIES"]["video"]["VALUE"]["TEXT"])?>" data-fancybox>
                                <svg class="video-more__icon-play"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-play"></use></svg>
                            </a>
                        </div>
                    </div>
                </div>
                <?}?>

               <!--<form class="hero-delivery-form wow slideInRight" action="/local/templates/main/php/form2.php" data-wow-duration="0.5s" data-wow-delay="0s"> -->
                   <!-- <div class="hero-delivery-form__title"> 
                       <?= htmlspecialcharsBack($arResult["PROPERTIES"]["form2_title"]["VALUE"]["TEXT"])?>
                    </div>-->
                   <!-- <h5 class="hero-delivery-form__subtitle">
                        <?= htmlspecialcharsBack($arResult["PROPERTIES"]["form2_text"]["VALUE"]["TEXT"])?>
                    </h5>-->
                    <!--<div class="hero-delivery-form__container">
                        <input class="input hero-delivery-form__input" type="text" placeholder="Имя" name="name" required>
                        <input class="input hero-delivery-form__input" type="text" placeholder="Телефон" name="number" required>
                        <button class="btn btn--red hero-delivery-form__submit" type="submit">
                            Получить бесплатную доставку
                        </button>
                    </div>-->
                     <!--<input autocomplete="off" type="hidden" name="call-control" class="call-control" value="0">-->
                <!--</form>-->

            </div>
        </div>