<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="features__wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    <div class="features__container wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <a class="features-item features-item--<?=$arItem['PROPERTIES']['dop_class']['VALUE']?>" href="#" data-<?=$arItem['PROPERTIES']['dop_class']['VALUE']?>>
                                <div class="features-item__img-block">
                                    <img class="features-item__img" src="<? echo CFile::GetPath($arItem["PROPERTIES"]["icon"]["VALUE"]) ?>" alt="">
                                </div>
                                <h5 class="features-item__title">
                                    <?=htmlspecialcharsBack($arItem["NAME"])?>
                                </h5>
                            </a>
                        </div>
    
    
    
    
<?endforeach;?>
</div>