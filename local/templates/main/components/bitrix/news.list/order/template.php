<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="order-stage__container swiper-container wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">                        
                        <div class="order-stage__wrapper swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    
    
    <div class="order-stage__slide swiper-slide">
                                <div class="order-stage__item" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
                                    <div class="order-stage__text-block">
                                        <div class="order-stage__number">
                                            <?=$arItem['PROPERTIES']['num']['VALUE']?>
                                        </div>
                                        <div class="order-stage__content">
                                            <?=htmlspecialcharsBack($arItem["NAME"])?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
    
    
<?endforeach;?>
</div>
</div>