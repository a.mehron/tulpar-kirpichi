<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>



    <div class="modal modal-features modal-features--<?=$arItem['PROPERTIES']['dop_class']['VALUE']?>">
        <div class="modal-features__body" data-bg-url="<? echo CFile::GetPath($arItem["PROPERTIES"]["banner"]["VALUE"]) ?>" style="background-image: url();">
            <div class="modal-features__inner">
                <h2 class="modal-features__title">
                    <?=htmlspecialcharsBack($arItem['PROPERTIES']['title']['VALUE'])?>
                </h2>
                <h4 class="modal-features__subtitle">
                    <?=htmlspecialcharsBack($arItem["PROPERTIES"]["text"]["VALUE"]["TEXT"])?>
                </h4>
                <h4 class="modal-features__desc">
                    <?=$arItem['PROPERTIES']['form_title']['VALUE']?>
                </h4>
                <form class="form-item modal-features__form-item" action="/local/templates/main/php/form1.php" method="post">
                    <input class="form-item__input" type="text" name="number[]" placeholder="Телефон" required>
                    <button class="form-item__submit" type="submit">
                        <svg class="form-item__icon form-item__icon--arrow-right"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#icon-arrow-right"></use></svg>
                    </button>
                    <input autocomplete="off" type="hidden" name="call-control[0]" class="call-control" value="0">
                </form>
            </div>
        </div>
    </div>





<?endforeach;?>

