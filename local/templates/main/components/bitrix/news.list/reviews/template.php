<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews__container swiper-container">
                            <div class="reviews__wrapper swiper-wrapper wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    <div class="reviews__item swiper-slide">
                                    <div class="reviews__video-container">
                                        <a class="reviews__video-block" <? if(!empty($arItem['PROPERTIES']['video']['VALUE'])){?> href="<?=$arItem['PROPERTIES']['video']['VALUE']?>" data-fancybox <?}?>>
                                            <img class="reviews__item-overlay" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                            <? if(!empty($arItem['PROPERTIES']['video']['VALUE'])){?><svg class="reviews__icon-play"><use xlink:href="/local/templates/main/images/sprite.svg#icon-play"></use></svg><?}?>
                                        </a>
                                    </div>
                                    <div class="reviews__text-block">
                                        <svg class="reviews__icon-qoutes"><use xlink:href="/local/templates/main/images/sprite.svg#icon-qoutes"></use></svg>
                                        <div class="reviews__name">
                                            <?echo $arItem["NAME"]?>
                                        </div>
                                        <p class="reviews__text">
                                            <?echo $arItem["PREVIEW_TEXT"];?>
                                        </p>
                                        <? if(!empty($arItem['PROPERTIES']['logo']['VALUE'])){?>
                                        <a class="reviews__source-link" href="#">
                                            <img class="reviews__source" src="<? echo CFile::GetPath($arItem["PROPERTIES"]["logo"]["VALUE"]) ?>" alt="">
                                        </a>
                                        <?}?>
                                    </div>
                                </div>
    
    
    
<?endforeach;?>
	</div>
    
    <div class="reviews__slider-nav wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                                <button class="reviews__btn-nav reviews__btn-nav--prev">
                                    <svg class="reviews__btn-nav-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-left"></use></svg>
                                </button>
                               
                                <button class="reviews__btn-nav reviews__btn-nav--next">
                                    <svg class="reviews__btn-nav-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-right"></use></svg>
                                </button>
                            </div>
</div>
