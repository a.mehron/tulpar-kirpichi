<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="material__container swiper-container">
                        <div class="swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    <a class="material-item swiper-slide wow fadeInUp" href="#" data-material data-wow-duration="0.8s" data-wow-delay="0s">
                                <div class="material-item__img-block">
                                    <img class="material-item__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                </div>
                                <div class="material-item__text-block">
                                    <h6 class="material-item__name">
                                        <?echo $arItem["NAME"]?>
                                    </h6>
                                </div>
                            </a>
    
    
    
<?endforeach;?>
</div>
                        <div class="material__slider-pagination"></div>
                    </div>