<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="container swiper-container">
                                <div class="manufacturers__wrapper swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    
    <div class="manufacturers__container swiper-slide wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                                        <div class="manufacturer-item">
                                        <? if(!empty($arItem['PROPERTIES']['logo']['VALUE'])){?>
                                            <img class="manufacturer-item__logo" src="<? echo CFile::GetPath($arItem["PROPERTIES"]["logo"]["VALUE"]) ?>"/>
                                          <?}?>
                                            <? if(!empty($arItem['PROPERTIES']['sale']['VALUE'])){?>
                                            <div class="manufacturer-item__sticker manufacturer-item__sticker--sale">
                                                SALE
                                            </div>
                                            <?}?>
                                            <? if(!empty($arItem['PROPERTIES']['new']['VALUE'])){?>
                                            <div class="manufacturer-item__sticker manufacturer-item__sticker--new">
                                                NEW
                                            </div> 
                                            <?}?>                                           
                                            <div class="manufacturer-item__img-block">
                                                <img class="manufacturer-item__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                            </div>
                                            <div class="manufacturer-item__text-block">
                                                <h6 class="manufacturer-item__title">
                                                    <?echo $arItem["NAME"]?>
                                                </h6>
                                                <div class="manufacturer-item__spec">
                                                    <div class="manufacturer-item__spec-title">
                                                        Характеристики
                                                    </div>
                                                    <table class="product-item__specifications-table">
                                                        <? $count = 0; ?>
  															<? for($i = 0; $i < count($arItem["DISPLAY_PROPERTIES"]["val_titles"]["VALUE"]); $i++) {?>
                                                        <tr class="product-item__specifications-tr">
                                                            <th class="product-item__specifications-th">
                                                                <?=$value?><?=$arItem["DISPLAY_PROPERTIES"]["val_titles"]["VALUE"][$i]?>
                                                            </th>
                                                            <td class="product-item__specifications-td manufacturer-item__td">
                                                                <?=$value2?><?=$arItem["DISPLAY_PROPERTIES"]["val_vals"]["VALUE"][$i]?>
                                                            </td>
                                                        </tr>
                                                        <?}?>
                                                        
                                                        
                                                    </table>
                                                </div>
                                                <button class="btn btn--red manufacturer-item__price">
                                                    <?=$arItem['PROPERTIES']['price']['VALUE']?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
    
    
    
    
	
	
<?endforeach;?>
</div>
</div>