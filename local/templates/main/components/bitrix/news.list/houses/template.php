<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="gas-blocks__container swiper-container">
                            <div class="swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    <div class="gas-blocks__item swiper-slide wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                                    <img class="gas-blocks__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                </div>
    
    
	
<?endforeach;?>
</div>
<div class="gas-blocks__nav-block"> 
                                <button class="gas-blocks__btn-nav gas-blocks__btn-nav--prev">
                                    <svg class="gas-blocks__btn-nav-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-left"></use></svg>
                                </button>
                                <div class="gas-blocks__slider-pagination"></div>
                                <button class="gas-blocks__btn-nav gas-blocks__btn-nav--next">
                                    <svg class="gas-blocks__btn-nav-icon"><use xlink:href="/local/templates/main/images/sprite.svg#icon-arrow-slider-right"></use></svg>
                                </button>
                            </div>
</div>