<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="social__container swiper-container">
                            <div class="swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    
    
    
    <div class="social__item-container swiper-slide wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0s">
                                    <a class="social__item" href="#">
                                        <img class="social__item-img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                        <? if(!empty($arItem['PROPERTIES']['icon']['VALUE'])){?>
                                        <svg class="social__icon social__icon--<?=$arItem['PROPERTIES']['icon']['VALUE']?>"><use xlink:href="/local/templates/main/images/sprite.svg#icon-<?=$arItem['PROPERTIES']['icon']['VALUE']?>"></use></svg>
                                        <?}?>
                                    </a>
                                    <div class="social__desc"><?=htmlspecialcharsBack($arItem["NAME"])?></div>
                                </div>
    
    
    
<?endforeach;?>
	</div>
</div>
